import vue from 'vue'
import vuex from 'vuex'
import alert from './alert'
import dialog from './dialog';
import auth from './auth';

import VuexPersist from "vuex-persist"

const vuexPersist = new VuexPersist({
    key: "aplikasi-blog-ta",
    storage: localStorage
})

vue.use(vuex)

export default new vuex.Store({
    plugins:[vuexPersist.plugin],
    modules :{
        alert,
        dialog,
        auth,
    }
})