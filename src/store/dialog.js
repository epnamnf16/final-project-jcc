export default {
    namespaced : true,
    state : {
        status : false,
        component : '',
        params:{},
        typeForm: ''
    },
    mutations: {
        setStatus:(state,status)=>{
            state.status = status
        },
        setTypeForm:(state,typeForm)=>{
            state.typeForm = typeForm
        },
        setComponent:(state,{component,params}) => {

            state.component = component
            state.params = params
        },
    },
    actions :{
        setStatus:({commit},status)=>{
            commit('setStatus',status)
        },
        setComponent : ({commit},{component,params,typeForm})=>{
            commit('setComponent',{component,params})
            commit('setTypeForm',typeForm)
            commit('setStatus',true)
        },
    },
    getters :{
        status : state => state.status,
        component:state=>state.component,
        params:state=>state.params,
        typeForm:state=>state.typeForm,
    }
}